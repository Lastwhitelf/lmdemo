'use strict';

app.factory('userService', function($http, $location,$routeParams){
	return{
		list: function(){
			var users = $http.get('/api/users.php');
			return users;
		},
		one: function(){
			var user = $http.get('/api/users.php?id='+$routeParams.id);
			return user;
		}
	}
});