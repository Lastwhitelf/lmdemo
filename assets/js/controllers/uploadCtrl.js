'use strict';

app.controller('uploadCtrl', ['$scope', '$window', function($scope, $window, upload) {
	
	$scope.onSuccess = function (response) {
		$window.location.href = '/#/users';
	};
	
}]);