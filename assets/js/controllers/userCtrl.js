'use strict';

app.controller('userCtrl', ['$scope', '$routeParams', 'userService', function($scope, $routeParams, userService){
	
	if($routeParams.id){

		var userrequest = userService.one($routeParams.id);
		userrequest.then(function(response){
			//console.log(response.data);
			
			$scope.user = response.data[0];
		});
		
	}
	else {

		var userrequest = userService.list();
		userrequest.then(function(response){
			//console.log(response.data);
			
			$scope.users = response.data;
		});
		
	}
	

}]);