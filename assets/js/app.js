var app = angular.module('app', ['ngRoute', 'lr.upload']);

app.config(function($routeProvider){
	$routeProvider
	.when('/', {
		templateUrl: '/app/views/login.html',
		controller: 'loginCtrl'
	})
	.when('/home', {
		templateUrl: '/app/views/home.html',
		controller: 'homeCtrl'
	})
	.when('/users', {
		templateUrl: '/app/views/user-list.html',
		controller: 'userCtrl'
	})
	.when('/user/:id', {
		templateUrl: '/app/views/user-detail.html',
		controller: 'userCtrl'
	})
	.when('/upload', {
		templateUrl: '/app/views/user-upload.html',
		controller: 'uploadCtrl'
	})
	.otherwise({
		redirectTo: '/'
	});
});

app.run(function($rootScope, $location, loginService){
	//prevent going to homepage if not loggedin
	var routePermit = ['/home'];
	$rootScope.$on('$routeChangeStart', function(){
		if(routePermit.indexOf($location.path()) !=-1){
			var connected = loginService.islogged();
			connected.then(function(response){
				if(!response.data){
					$location.path('/');
				}
			});
			
		}
	});
	//prevent going back to login page if sessino is set
	var sessionStarted = ['/'];
	$rootScope.$on('$routeChangeStart', function(){
		if(sessionStarted.indexOf($location.path()) !=-1){
			var cantgoback = loginService.islogged();
			cantgoback.then(function(response){
				if(response.data){
					$location.path('/home');
				}
			});
		}
	});
});