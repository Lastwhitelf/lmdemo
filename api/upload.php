<?php

session_start();

include("config.php");

$everything_is_ok = false;

if ($_FILES['file']) {
	$file = $_FILES['file']['tmp_name'];                     //    Get Temporary filename
	if ($file) {

		$handle = fopen($file,"r");                     //  Open the file and read
		while($strUserData = fgets($handle, 4096)) {
			$explode = explode("|",$strUserData);
			$strDatas[] = $explode;
			
			$strTableColumn = count($explode);
		}
		
		if ($strDatas) {
			$strInsertRecords = 0;
			$strDuplicationRecords = 0;
			$strDuplicateData = array();
			$strDuplicateMail = "";
			
			//print_r($strTableColumn);
			
			if ($strTableColumn == 5) {
				for($k=0; $k<count($strDatas); $k++) {
					$strStatus = 0; //checkDuplicate($strDatas[$k]['4']);
					if ($strStatus == 0) {
						
						$strData = $strDatas[$k];
						
						$strMessage = insertUser($conn, $strData['0'], $strData['1'], $strData['2'], $strData['3'], $strData['4']);
						
						$strInsertRecords++;
					} else {
						$strDuplicationRecords++;
						$strDuplicateData[$strDuplicationRecords] = $strDatas[$k]['2'];
						$strDuplicateMail.= $strDuplicateData[$strDuplicationRecords]. "\n";
					}
				}
				if  (count($strDatas)-1 == $strInsertRecords) {
					$everything_is_ok = true;
					$strMessage = 'User record(s) inserted successfully!';
					$strClass = 'Success';
				}
				if  (count($strDatas)-1 != $strInsertRecords) {
					$everything_is_ok = true;
					$strMessage = 'User record(s) inserted successfully but some of record(s) are already exists.!';
					$strClass = 'Error';
				}
				if  (count($strDatas)-1 == $strDuplicationRecords) {
					$strMessage = 'User record(s) are already exists.!';
					$strClass = 'Error';
				}
			} else {
				$strMessage = 'Column mis-match, Please verify the file.';
				$strClass = 'Error';
			}
		}
	} else {
		$strMessage = 'Please upload a valid file.';
		$strClass = 'Error';
	}
}

if ($everything_is_ok)
{
	header('Content-Type: application/json');
	print json_encode(array('message' => $strMessage));
}
else
{
	header('HTTP/1.1 500 Internal Server Booboo');
	header('Content-Type: application/json; charset=UTF-8');
	die(json_encode(array('message' => $strMessage)));
}

function checkDuplicate($conn,$name,$surname,$birthdate,$city,$fiscalcode) {
	
	//echo ("insertUser");
	
	$sql = "INSERT INTO users (name, surname, birthday, city, fiscalcode) VALUES ('$name','$surname','$birthdate','$city','$fiscalcode')";
	
	if ($conn->query($sql) === TRUE) {
		return "New record created successfully";
	} else {
		return "Error: " . $sql . "<br>" . $conn->error;
	}
	
	$conn->close();
	
}

function insertUser($conn,$name,$surname,$birthdate,$city,$fiscalcode) {
	
	//echo ("insertUser");
	
	$sql = "INSERT INTO users (name, surname, birthday, city, fiscalcode) VALUES ('$name','$surname','$birthdate','$city','$fiscalcode')";
	
	if ($conn->query($sql) === TRUE) {
		return "New record created successfully";
	} else {
		return "Error: " . $sql . "<br>" . $conn->error;
	}
	
	$conn->close();
	
}

?>