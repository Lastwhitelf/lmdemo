-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versione server:              10.1.36-MariaDB - mariadb.org binary distribution
-- S.O. server:                  Win32
-- HeidiSQL Versione:            10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dump della struttura del database lmdemo
CREATE DATABASE IF NOT EXISTS `lmdemo` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `lmdemo`;

-- Dump della struttura di tabella lmdemo.sys_users
CREATE TABLE IF NOT EXISTS `sys_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` char(50) NOT NULL DEFAULT '0',
  `password` char(50) NOT NULL DEFAULT '0',
  `name` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dump dei dati della tabella lmdemo.sys_users: ~1 rows (circa)
/*!40000 ALTER TABLE `sys_users` DISABLE KEYS */;
INSERT INTO `sys_users` (`id`, `email`, `password`, `name`) VALUES
	(1, 'info@lmdemo.local', '1234', 'Riccardo');
/*!40000 ALTER TABLE `sys_users` ENABLE KEYS */;

-- Dump della struttura di tabella lmdemo.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `surname` varchar(50) CHARACTER SET latin1 NOT NULL,
  `birthday` varchar(100) CHARACTER SET latin1 NOT NULL,
  `city` varchar(50) CHARACTER SET latin1 NOT NULL,
  `fiscalcode` varchar(200) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=utf8;

-- Dump dei dati della tabella lmdemo.users: ~12 rows (circa)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `created`, `name`, `surname`, `birthday`, `city`, `fiscalcode`) VALUES
	(169, '2014-08-31 18:21:20', 'Franco', 'Cara', '1/4/1985', 'Roma', 'XXXYYY00X00Y000Z'),
	(170, '2014-08-31 18:30:58', 'Elisa', 'Severi', '2/2/1975', 'Milano', 'XXXYYY00X00Y000Z'),
	(171, '2014-08-31 18:32:03', 'Tamara', 'Fedeli', '4/9/1979', 'Torino', 'XXXYYY00X00Y000Z'),
	(172, '2014-08-31 20:34:21', 'Rossano', 'Marchini', '11/04/1990', 'Roma', 'XXXYYY00X00Y000Z'),
	(173, '2014-08-31 20:36:02', 'Elvira', 'Galli', '5/3/1986', 'Rimini', 'XXXYYY00X00Y000Z'),
	(174, '2014-08-31 20:44:54', 'Marco Antonio', 'Sabatini', '5/8/1985', 'Lecco', 'XXXYYY00X00Y000Z'),
	(175, '2014-08-31 20:45:08', 'Beatrice', 'Rosa', '16/7/1992', 'Afragola', 'XXXYYY00X00Y000Z'),
	(178, '2014-08-31 21:00:26', 'Davide', 'Cicco', '14/9/2000', 'Trieste', 'XXXYYY00X00Y000Z'),
	(187, '2019-04-14 14:49:13', 'Giovanni', 'Franceschini', '13/12/1982', 'Lucca', 'XXXYYY00X00Y000Z'),
	(188, '2019-04-14 14:56:00', 'Guglielmo', 'Rossi', '14/6/1994', 'Posillipo', 'XXXYYY00X00Y000Z');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
